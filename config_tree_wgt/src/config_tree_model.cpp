#define QT_NO_DEBUG_OUTPUT
#define QT_NO_INFO_OUTPUT
#define QT_NO_WARNING_OUTPUT


#include <QtWidgets>

#include "config_tree_model.h"
#include "config_tree_item.h"
#include "config_tree_delegate.h"


ConfigTreeModel::ConfigTreeModel(QObject* parent, ValueComplex::PTR_T entry)
	: QAbstractItemModel(parent)
	, m_treeView(nullptr)
{
    m_entry = entry;
    m_rootItem = new ConfigTreeItem(this, nullptr, entry);

    setupData(entry, m_rootItem);
}

ConfigTreeModel::~ConfigTreeModel()
{
    delete m_rootItem;
}

void ConfigTreeModel::onDataBaseChanged()
{
    delete m_rootItem;

    m_rootItem = new ConfigTreeItem(this);
    setupData(m_entry, m_rootItem);
}

void ConfigTreeModel::onChildAdded(ValueBase::PTR_T parent, ValueBase::PTR_T child)
{

}

int ConfigTreeModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_rootItem->columnCount();
}

// override from base 
QVariant ConfigTreeModel::data(const QModelIndex& index, int role) const
{

    if (!index.isValid()) {
        qInfo("Index invalid !");
        return QVariant();
    }

    ConfigTreeItem* i = getItem(index);
    ValueBase::PTR_T p = i->getValue();

    if (p == NULL) {
        qInfo("Value is null ...");
    }
    else {
        if (role == Qt::DisplayRole) {
            qInfo("Value Name: %s:%s r:%d c:%d ",
                p->getTypeName().c_str(),
                p->getValueName().c_str(),
                index.row(), index.column());
        }
    }

    if (role == Qt::ToolTipRole) {
        std::string usrHint;

        if (p->getProperty(ValueBase::wpHint, usrHint)) {
            return QVariant(QString(usrHint.c_str()));
        }

        return QVariant();
    }

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    ConfigTreeItem* item = getItem(index);
   
    return item->data(index.column());
}

// override from base
Qt::ItemFlags ConfigTreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    ConfigTreeItem* p = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());
    ValueBase::PTR_T val = p->getValue();

    if (val == NULL) {
        return Qt::NoItemFlags;
    }

    if (val->getType() == VALUE_TYPE::COMPLEX) {
        // not editable at all 
        return QAbstractItemModel::flags(index);
    }

    if (index.column() == 2) {
        // let's have a look if we are in setup mode

        //if (NGS_ApplicationParameter::instance().m_parSetupMode.get()) {
        //    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
        //}
        return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    }

    return QAbstractItemModel::flags(index);
}

ConfigTreeItem* ConfigTreeModel::getItem(const QModelIndex& index) const
{
    if (index.isValid()) {
        ConfigTreeItem* item = static_cast<ConfigTreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return m_rootItem;
}

// override from base
QVariant ConfigTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return m_rootItem->data(section);

    return QVariant();
}

// override from base
QModelIndex ConfigTreeModel::index(int row, int column, const QModelIndex& parent) const
{
    // only column 0 has childs
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    ConfigTreeItem* parentItem = getItem(parent);
    if (!parentItem)
        return QModelIndex();

    ConfigTreeItem* childItem = parentItem->child(row);
    if (childItem) {
        return createIndex(row, column, childItem);
    }
    return QModelIndex();
}

// override from base
bool ConfigTreeModel::insertColumns(int position, int columns, const QModelIndex& parent)
{
#ifdef __unused__
    beginInsertColumns(parent, position, position + columns - 1);
    const bool success = m_rootItem->insertColumns(position, columns);
    endInsertColumns();

    return success;
#endif

    return false;
}

// override from base
bool ConfigTreeModel::insertRows(int position, int rows, const QModelIndex& parent)
{
#ifdef __UNUSED__
    ConfigTreeItem* parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginInsertRows(parent, position, position + rows - 1);
    const bool success = parentItem->insertChildren(position,
        rows,
        m_rootItem->columnCount());
    endInsertRows();

    return success;
#endif

	beginInsertRows(parent, position, position + rows - 1);
	endInsertRows();

    return true;
}

// override from base
QModelIndex ConfigTreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    ConfigTreeItem* item = getItem(index);
    ConfigTreeItem* parentItem = item ? item->parent() : nullptr;

    if (parentItem == m_rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

// override from base
bool ConfigTreeModel::removeColumns(int position, int columns, const QModelIndex& parent)
{
    beginRemoveColumns(parent, position, position + columns - 1);
    const bool success = m_rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (m_rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

// override from base
bool ConfigTreeModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    ConfigTreeItem* parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    const bool success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

// override from base
int ConfigTreeModel::rowCount(const QModelIndex& index) const
{
    const ConfigTreeItem* item = getItem(index);

    return item ? item->childCount() : 0;
}

// override from base
bool ConfigTreeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (role != Qt::EditRole)
        return false;

    ConfigTreeItem* item = getItem(index);
    bool result = item->setData(index.column(), value);

    if (result)
        emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });

    return result;
}

// override from base
bool ConfigTreeModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    const bool result = m_rootItem->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

void ConfigTreeModel::setupData(ValueComplex::PTR_T entry, ConfigTreeItem* parent)
{
    if (entry->lockAccess()) {
        for (int n = 0; n < entry->getChildCount(); n++) {
            ValueBase::PTR_T p = entry->getChild(n);
            bool hideChilds = false;
            p->getProperty(ValueBase::wpHideChilds, hideChilds);

            ConfigTreeItem* child = parent->appendChild(p, this);

            if (p->getType() == VALUE_TYPE::COMPLEX) {
                setupData(ValueComplex::cast(p), child);
            }
            else {
                if (hideChilds) {
                    continue;
                }
                if (p->getChildCount()) {
                    for (int n = 0; n < p->getChildCount(); n++) {
                        ValueBase::PTR_T val = p->getChild(n);
                        child->appendChild(val, this);
                    }
                }
            }
        }
        entry->unlock();
    }
}

void ConfigTreeModel::removeItem(int row, int column, ConfigTreeItem* parentItem)
{
	//removeNode(row, item->parent());
    //removeRow(row, createIndex(0, 0, item->parent()));

	removeNode(row, parentItem);
	//removeRow(row, createIndex(0, 0, item->parent()));

}

void ConfigTreeModel::removeNode(int row, ConfigTreeItem* parent)
{
    beginResetModel();
    removeRow(row, createIndex(0, 0, parent));

    //const bool hasCurrent = m_treeView->selectionModel()->currentIndex().isValid();
    //insertRowAction->setEnabled(hasCurrent);
    //insertColumnAction->setEnabled(hasCurrent);

    //if (hasCurrent) {
    //    m_treeView->closePersistentEditor(m_treeView->selectionModel()->currentIndex());
    //}

    endResetModel();
}

void ConfigTreeModel::setView(QTreeView* v)
{
    m_treeView = v;
    m_treeView->setModel(this);
}

bool ConfigTreeModel::insertRows(int position, int rows, ConfigTreeItem* parent)
{
	QModelIndex parentIndex;

	if (parent != nullptr) {
		parentIndex = createIndex(0, 0, parent);
	}
	else {
		parentIndex = QModelIndex();
	}
	
	return insertRows(position, rows, parentIndex);
}

void ConfigTreeModel::valueChanged(int row, int column, ConfigTreeItem* item)
{
    ValueBase::PTR_T p = item->getValue();

    if (p == NULL) {
        qInfo("value is null ...");
    }
    else {
        qInfo("parent Value Name: %s:%s row: %d column: %d", 
            p->getTypeName().c_str(), 
            p->getValueName().c_str(),
            row, column);
    }

    QModelIndex start = createIndex(row, column, item);
    QModelIndex stop = createIndex(row, column, item);

    emit dataChanged(start, stop);
}


