#include "config_tree_delegate.h"
#include "config_tree_item.h"
#include "parameter_value.h"
#include "qcombobox.h"
#include "qcheckbox.h"
#include "qpainter.h"
#include "qlineedit.h"
#include "qvalidator.h"
#include "qaction.h"
#include "qfiledialog.h"
//#include "parameter.h"

void ConfigTreeDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	ConfigTreeItem* item = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());
	ValueBase::PTR_T p = item->getValue();

#ifdef __UNUSED__
	switch (p->getType()) {
	case VALUE_TYPE::BOOL:
		//painter->drawText(option.rect, "X");
		QStyledItemDelegate::paint(painter, option, index);
		break;

	default:
		QStyledItemDelegate::paint(painter, option, index);
		break;
	}
#endif

	QStyledItemDelegate::paint(painter, option, index);
}

QSize ConfigTreeDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	ConfigTreeItem* item = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());

	if (item == NULL) {
		return QStyledItemDelegate::sizeHint(option, index);
	}

	ValueBase::PTR_T p = item->getValue();

	if (p == NULL) {
		return QStyledItemDelegate::sizeHint(option, index);
	}

	switch (p->getType()) {
	case VALUE_TYPE::BOOL:
		return QStyledItemDelegate::sizeHint(option, index);
		break;

	default:
		return QStyledItemDelegate::sizeHint(option, index);
	}

}

QWidget* ConfigTreeDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	ConfigTreeItem* item = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());
	ValueBase::PTR_T p = item->getValue();

	GuiEditorType editorType;
	uint32_t editorIdx;

	if (p->getProperty(ValueBase::wpTreeItemEditor, editorIdx)) {
		editorType = static_cast<GuiEditorType>(editorIdx);

		switch (editorType) {
		case GuiEditorType::PathSelection:
		{
			ConfigTreeSelectPath* editor = new ConfigTreeSelectPath(parent);
			editor->setProperty(ValueBase::wpTreeItemEditor, QVariant(static_cast<uint32_t>(editorType)));
			return editor;
		}

		case GuiEditorType::FileSelection:
		{
			ConfigTreeSelectFile* editor = new ConfigTreeSelectFile(parent);
			editor->setProperty(ValueBase::wpTreeItemEditor, QVariant(static_cast<uint32_t>(editorType)));
			return editor;
		}
		}

	}

	switch (p->getType())
	{
	case VALUE_TYPE::DOUBLE:
	{
		// create QLineEdit with QDoubleValidator
		QLineEdit* le = new QLineEdit(parent);
		le->setValidator(new QDoubleValidator(le));
		return le;
	}

	case VALUE_TYPE::FLOAT:
	{
		// create QLineEdit with QDoubleValidator
		QLineEdit* le = new QLineEdit(parent);
		le->setValidator(new QDoubleValidator(le));
		return le;
	}

	case VALUE_TYPE::SELECTION:
	{
		QComboBox* box = new QComboBox(parent);
		std::vector<std::string>entrys = ValueSelection::cast(p)->getItems();

		for (auto item : entrys) {
			box->addItem(QString(item.c_str()));
		}

		ValueBase::PTR_T selected = ValueSelection::cast(p)->getSelected();

		if (selected != nullptr) {
			box->setCurrentIndex(ValueSelection::cast(p)->getSelectedIdx());
		}
		connect(box, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));
		return box;
	}

	case VALUE_TYPE::BOOL:
	{
		QCheckBox* checkBox = new QCheckBox(parent);
		checkBox->setAutoFillBackground(true);
		return checkBox;
	}

	case VALUE_TYPE::STRING:
	{
		QLineEdit* lineEdit = new QLineEdit(parent);
		return lineEdit;
	}

	default:
		return QStyledItemDelegate::createEditor(parent, option, index);
	}
}

void ConfigTreeDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	ConfigTreeItem* item = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());
	ValueBase::PTR_T p = item->getValue();

	GuiEditorType	editorType;
	uint32_t		editorIdx;

	if (p->getProperty(ValueBase::wpTreeItemEditor, editorIdx)) {
		editorType = static_cast<GuiEditorType>(editorIdx);

		switch (editorType) {
		case GuiEditorType::PathSelection:
			if (ConfigTreeSelectPath::State::Init == qobject_cast<ConfigTreeSelectPath*>(editor)->getState()) {
				QString newPath = QFileDialog::getExistingDirectory(nullptr,
					"Select directory", Value<std::string>::cast(p)->get().c_str());

				qobject_cast<ConfigTreeSelectPath*>(editor)->setState(ConfigTreeSelectPath::State::DialogDone);
				qobject_cast<ConfigTreeSelectPath*>(editor)->setText(newPath);
			}
			return;

		case GuiEditorType::FileSelection:
			if (ConfigTreeSelectFile::State::Init == qobject_cast<ConfigTreeSelectFile*>(editor)->getState()) {
				QString newFile = QFileDialog::getOpenFileName(nullptr,
					"Select driver dll file", Value<std::string>::cast(p)->get().c_str());

				qobject_cast<ConfigTreeSelectFile*>(editor)->setState(ConfigTreeSelectFile::State::DialogDone);
				qobject_cast<ConfigTreeSelectFile*>(editor)->setText(newFile);
			}
			return;
		}
	}

	switch (p->getType()) {
	case VALUE_TYPE::SELECTION:
		{
			ValueBase::PTR_T selected = ValueSelection::cast(p)->getSelected();
			if (selected != nullptr) {
				ValueSelection::cast(p)->setSelected(selected);
			}
			break;
		}

	case VALUE_TYPE::DOUBLE:
		if (QString("QLineEdit") == editor->metaObject()->className()) {
			dynamic_cast<QLineEdit*>(editor)->setText(QLocale().toString(Value<double>::cast(p)->get()));
		}
		break;

	case VALUE_TYPE::BOOL:
		if ((Value<bool>::cast(p))->get()) {
			dynamic_cast<QCheckBox*>(editor)->setCheckState(Qt::Checked);
		}
		else {
			dynamic_cast<QCheckBox*>(editor)->setCheckState(Qt::Unchecked);
		}
		break;

	default:
		QStyledItemDelegate::setEditorData(editor, index);
		break;
	}

	
}

void ConfigTreeDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	ConfigTreeItem* item = reinterpret_cast<ConfigTreeItem*>(index.internalPointer());
	ValueBase::PTR_T p = item->getValue();

	if (p->getType() == VALUE_TYPE::SELECTION) {	
		ValueSelection::cast(p)->setSelectedIdx(dynamic_cast<QComboBox*>(editor)->currentIndex());
		return;
	}

	QStyledItemDelegate::setModelData(editor, model, index);
}

void ConfigTreeDelegate::commitAndCloseEditor()
{
	
}

void ConfigTreeDelegate::onCurrentIndexChanged(int n)
{

}


ConfigTreeSelectPath::ConfigTreeSelectPath(QWidget *parent)
	: QLineEdit(parent)
{
	m_state = State::Init;
}

ConfigTreeSelectFile::ConfigTreeSelectFile(QWidget *parent)
	: QLineEdit(parent)
{
	m_state = State::Init;
}