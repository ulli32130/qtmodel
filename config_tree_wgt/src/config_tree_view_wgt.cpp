#include "qclipboard.h"
#include "qmessagebox.h"
#include "./ui_config_tree_view_wgt.h"
#include "config_tree_view_wgt.h"
#include "config_tree_model.h"
#include "config_tree_item.h"
#include "config_tree_delegate.h"
#include "qmessagebox.h"

ConfigTreeViewWgt::ConfigTreeViewWgt(QWidget* parent, ValueComplex::PTR_T entry)
	: ConfigTreeViewWgt(parent)
{
	setEntryNode(entry);
}

ConfigTreeViewWgt::ConfigTreeViewWgt(QWidget* parent)
	: QWidget(parent)
	, m_model(nullptr)
	, ui(new Ui::ConfigTreeViewWidget)
{
	ui->setupUi(this);
}

void ConfigTreeViewWgt::setEntryNode(ValueComplex::PTR_T entry)
{
	m_model = new ConfigTreeModel(this, entry);

	ui->m_treeView->setItemDelegate(new ConfigTreeDelegate(m_model));
	ui->m_treeView->setModel(m_model);
	ui->m_treeView->resizeColumnToContents(0);
	ui->m_treeView->insertAction(nullptr, ui->actionCopy_parameter_name_to_clipboard);
	ui->m_treeView->insertAction(nullptr, ui->actionShowHideChilds);
	ui->m_treeView->insertAction(nullptr, ui->actionDelete_entry);

	ui->m_treeView->resizeColumnToContents(0);
	connect(ui->m_treeView, &QTreeView::expanded, this, &ConfigTreeViewWgt::onExpanded);
}

ConfigTreeItem* ConfigTreeViewWgt::selectedItem()
{
	QModelIndex idx = ui->m_treeView->currentIndex();
	ConfigTreeItem *item = static_cast<ConfigTreeItem *>(idx.internalPointer());

	if (item == nullptr) {
		QMessageBox::warning(nullptr, "Empty selection", "Nothing selected !!",
			QMessageBox::StandardButton::Ok);
		return nullptr;
	}

	return item;
}

void ConfigTreeViewWgt::onCopyParameterNameToClipboard()
{
	QClipboard *clipboard = QGuiApplication::clipboard();

	QModelIndex idx = ui->m_treeView->currentIndex();
	ConfigTreeItem *item = static_cast<ConfigTreeItem *>(idx.internalPointer());

	if (item == nullptr) {
		QMessageBox::warning(nullptr, "Empty selection", "Nothing selected !!",
			QMessageBox::StandardButton::Ok);
		return;
	}

	std::string pathName;
	item->path(pathName);

	pathName.insert(0, "\"");
	pathName += "\"";

	clipboard->setText(pathName.c_str());
}

void ConfigTreeViewWgt::onHideChilds()
{
	QModelIndex idx = ui->m_treeView->currentIndex();
	ConfigTreeItem *item = static_cast<ConfigTreeItem *>(idx.internalPointer());

	if (item == nullptr) {
		QMessageBox::warning(nullptr, "Empty selection", "Nothing selected !!",
			QMessageBox::StandardButton::Ok);
		return;
	}

	ValueBase::PTR_T node = item->getValue();
	bool viewState;
	if (node->getProperty(ValueBase::wpHideChilds, viewState)) {
		viewState = !viewState;
		node->setProperty(ValueBase::wpHideChilds, viewState);
	}
	else {
		// property was not yes set, that means the children are visible, set proprty to false !
		viewState = false;
		node->setProperty(ValueBase::wpHideChilds, viewState);
	}
}

void ConfigTreeViewWgt::onDeleteEntry()
{
	ConfigTreeItem *item = selectedItem();

	if (item == nullptr) {

		return;
	}

	if (QMessageBox::StandardButton::Yes == QMessageBox::warning(nullptr, "Warning", "Do you really wan't to delete this entry ??",
		QMessageBox::StandardButton::Yes, QMessageBox::StandardButton::No)) {

		QMessageBox::information(nullptr, " ..... sorry", "Not yet implemented ....",
			QMessageBox::StandardButton::Ok);

		//item->removeChildren();
	}
}

void ConfigTreeViewWgt::onExpanded()
{
	ui->m_treeView->resizeColumnToContents(0);
}
