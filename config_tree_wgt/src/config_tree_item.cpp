/*
    treeitem.cpp

    A container for items of data supplied by the simple tree model.
*/

#define QT_NO_DEBUG_OUTPUT
#define QT_NO_INFO_OUTPUT
#define QT_NO_WARNING_OUTPUT

#include "config_tree_item.h"
#include "parameter_value.h"
#include "config_tree_model.h"

#include <QAbstractItemModel>
#include <qlocale.h>

ConfigTreeItem::ConfigTreeItem(ConfigTreeModel* model, ConfigTreeItem* parent, ValueBase::PTR_T value)
    : m_model(model)
    , m_hideChilds(false)
    , m_parentItem(parent)
    , m_valueBase(value)
{
    if (parent == NULL) 
    {
        m_isRootItem = true;
    }
    else 
    {
        m_isRootItem = false;
    }
  
    if (m_valueBase) 
    {
        m_valueBase->getProperty(ValueBase::wpHideChilds, m_hideChilds);
        m_valueName = m_valueBase->getValueName();

		//m_onChildAdded.connect(m_valueBase, std::bind(&ConfigTreeItem::appendChildXX, this, std::placeholders::_1, std::placeholders::_2));

        m_onChildAdded.connect(m_valueBase, [this](ValueBase::PTR_T parent, ValueBase::PTR_T child)
        {
            if (!m_hideChilds) 
            {
                ConfigTreeItem* childItem = appendChild(child, m_model);
            }
        });

        m_onChanged.connect(m_valueBase, [this](ValueBase::PTR_T p)
        {
            m_model->valueChanged(childNumber(), 2, this);
        });
    }

    //m_onValueDestroyed = [this](ValueBase::PTR_T p) {
    //    m_valueBase = NULL;
    //    if (m_parentItem != NULL) {
    //        m_model->removeNode(childNumber(), m_parentItem);
    //    }
    //};
}

ConfigTreeItem::~ConfigTreeItem()
{
    qDeleteAll(m_childItems);
}

void ConfigTreeItem::appendChildXX(ValueBase::PTR_T node, ValueBase::PTR_T child)
{

}

ConfigTreeItem* ConfigTreeItem::child(int number)
{
    if (number < 0 || number >= m_childItems.size())
        return nullptr;
    return m_childItems.at(number);
}

int ConfigTreeItem::childCount() const
{
    return m_childItems.count();
}

int ConfigTreeItem::childNumber() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<ConfigTreeItem*>(this));
    return 0;
}

int ConfigTreeItem::columnCount() const
{
    // all rows have three columns
    return 3;
}

QVariant ConfigTreeItem::data(ValueBase::PTR_T value) const
{
    if (value == nullptr) {
        return QVariant();
    }

    switch (value->getType()) {
    case VALUE_TYPE::INT8:
        return QVariant(Value<int8_t>::cast(value)->get());

    case VALUE_TYPE::UINT8:
        return QVariant(Value<uint8_t>::cast(value)->get());

    case VALUE_TYPE::INT16:
        return QVariant(Value<int16_t>::cast(value)->get());

    case VALUE_TYPE::UINT16:
        return QVariant(Value<uint16_t>::cast(value)->get());

    case VALUE_TYPE::INT32:
        return QVariant(Value<int32_t>::cast(value)->get());

    case VALUE_TYPE::UINT32:
        return QVariant(Value<uint32_t>::cast(value)->get());

    case VALUE_TYPE::INT64:
        return QVariant::fromValue(Value<int64_t>::cast(value)->get());

    case VALUE_TYPE::UINT64:
        return QVariant::fromValue(Value<uint64_t>::cast(value)->get());

    case VALUE_TYPE::FLOAT:
        return QVariant(Value<float>::cast(value)->get());

    case VALUE_TYPE::DOUBLE:
        return QVariant(Value<double>::cast(value)->get());

    case VALUE_TYPE::BOOL:
        return QVariant(Value<bool>::cast(value)->get());

    case VALUE_TYPE::STRING:
        return QVariant(Value<std::string>::cast(value)->get().c_str());

    case VALUE_TYPE::COMPLEX:
        // empty, complex node has no data to show ??
        return QVariant(" ");

    case VALUE_TYPE::SELECTION:
    {
		ValueBase::PTR_T selected = ValueSelection::cast(value)->getSelected();

        if (selected.operator->() == nullptr) {
            return QVariant();
        }
        return data(selected);
    }

    default:
        return QVariant("-- NOT YET IMPLEMENTED --");
    }
}

QVariant ConfigTreeItem::data(int column) const
{
    std::string caption;

    if (column < 0 || column >= 3)
        return QVariant();

    if (isRootItem()) {
        // root item give header
        switch (column) {
        case 0:
            return QVariant("Name");
        case 1:
            return QVariant("Type");
        case 2:
            return QVariant("Value");
        default:
            return QVariant("Should not occur !!");
        }
    }

    switch (column) {

    case 0:
    {
        std::string caption("");
        if (!m_valueBase->getProperty<std::string>(ValueBase::wpCaption, caption)) {
            // no caption given, we use the value name as caption for tree enrtry
            caption = m_valueBase->getValueName();
        }
        return QVariant(caption.c_str());
    }
    case 1:
        return QVariant(m_valueBase->getTypeName().c_str());

    case 2:
        return data(m_valueBase);

    default:
        return QVariant("-- should not occur !! --");
    }

}

void ConfigTreeItem::setSelectedData(ConfigTreeItem* item, ValueSelection::PTR_T selection)
{
	ValueBase::PTR_T value = selection->getSelected();
    if (value != nullptr) {
        item->setData(2, value->getValueName().c_str());
    }
}

ConfigTreeItem* ConfigTreeItem::appendChild(ValueBase::PTR_T value, ConfigTreeModel* model)
{
    bool hideChilds = false;

    ConfigTreeItem* item = new ConfigTreeItem(model, this, value);

    m_childItems.append(item);

	if (nullptr == parent()) {
		model->insertRows(0, 1, nullptr);
	}
	else {
		model->insertRows(parent()->childCount(), 1, parent());
	}

    return item;
}

ConfigTreeItem* ConfigTreeItem::appendChild()
{
    ConfigTreeItem* child = new ConfigTreeItem(m_model, this);
    m_childItems.append(child);

    return child;
}

bool ConfigTreeItem::insertColumns(int position, int columns)
{
#ifdef __UNUSED__
    if (position < 0 || position > m_itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        m_itemData.insert(position, QVariant());

    for (ConfigTreeItem* child : qAsConst(childItems))
        child->insertColumns(position, columns);

    return true;
#endif
    return false;
}

ConfigTreeItem* ConfigTreeItem::parent()
{
    return m_parentItem;
}

bool ConfigTreeItem::removeChildren()
{
    for (ConfigTreeItem* child : m_childItems) {
        child->removeChildren();
    }

    if (m_parentItem != NULL) {
        m_model->removeItem(childNumber(), 3, m_parentItem);
    }

    return true;
}

bool ConfigTreeItem::removeChild(ConfigTreeItem* child)
{
    for (int n = 0; n < m_childItems.count(); n++) {
        if (m_childItems[n] == child) {
            delete m_childItems.takeAt(n);
            return true;
        }
    }

    return false;
}

bool ConfigTreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > m_childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete m_childItems.takeAt(position);

    return true;
}

bool ConfigTreeItem::removeColumns(int position, int columns)
{
#ifdef __UNUSED__
    if (position < 0 || position + columns > m_itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        m_itemData.remove(position);

    for (ConfigTreeItem* child : qAsConst(childItems))
        child->removeColumns(position, columns);

    return true;
#endif

    return false;
}

bool ConfigTreeItem::setData(const QVariant& variant)
{
    switch (m_valueBase->getType()) {
    case VALUE_TYPE::INT8:
		return Value<int8_t>::cast(m_valueBase)->set(variant.value<int8_t>());

    case VALUE_TYPE::UINT8:
        return Value<uint8_t>::cast(m_valueBase)->set(variant.value<uint8_t>());

    case VALUE_TYPE::INT16:
        return Value<int16_t>::cast(m_valueBase)->set(variant.value<int16_t>());

    case VALUE_TYPE::UINT16:
        return Value<uint16_t>::cast(m_valueBase)->set(variant.value<uint16_t>());

    case VALUE_TYPE::INT32:
        return Value<int32_t>::cast(m_valueBase)->set(variant.value<int32_t>());

    case VALUE_TYPE::UINT32:
        return Value<uint32_t>::cast(m_valueBase)->set(variant.value<uint32_t>());

    case VALUE_TYPE::INT64:
        return Value<int64_t>::cast(m_valueBase)->set(variant.value<int64_t>());

    case VALUE_TYPE::UINT64:
        return Value<uint64_t>::cast(m_valueBase)->set(variant.value<uint64_t>());

    case VALUE_TYPE::FLOAT: 
        return Value<float>::cast(m_valueBase)->set(QLocale().toFloat(variant.toString()));

    case VALUE_TYPE::DOUBLE:
        if (QVariant::Type::Double == variant.type()) {
            return Value<double>::cast(m_valueBase)->set(variant.value<double>());
        } 
        return Value<double>::cast(m_valueBase)->set(QLocale().toDouble(variant.toString()));

    case VALUE_TYPE::BOOL:
        return Value<bool>::cast(m_valueBase)->set(variant.value<bool>());

    case VALUE_TYPE::STRING:
        return Value<std::string>::cast(m_valueBase)->set(variant.value<QString>().toStdString().c_str());

    case VALUE_TYPE::COMPLEX:
        // empty, complex node has no data to show ??
        break;

    case VALUE_TYPE::SELECTION:
        //setSelectedData(item, dynamic_cast<ValueSelection*>(value));
        break;

    default:
       
        break;
    }

    return false;
}

bool ConfigTreeItem::setData(int column, const QVariant& value)
{
    if (column == 2) {
        setData(value);
        return true;
    }
    
    return false;

#ifdef __UNUSED__
    if (column < 0 || column >= m_itemData.size())
        return false;

    m_itemData[column] = value;
    return true;
#endif
}

void ConfigTreeItem::path(std::string& pathName)
{
	if (m_valueBase != nullptr) {
		m_valueBase->path(pathName);
	}
}
