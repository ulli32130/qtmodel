#pragma once

#include <QMainWindow>
#include <QDialog>
#include <QVBoxLayout>
#include <QWidget>
#include <qpushbutton.h>
#include <QGraphicsView>
#include <QGraphicsItem>

#include "value_complex.h"
#include "value_selection.h"

#include "config_tree_export.h"

QT_BEGIN_NAMESPACE
namespace Ui { class ConfigTreeViewWidget; }
QT_END_NAMESPACE

class ConfigTreeModel;
class ConfigTreeItem;

class LIBCLASS_EXPORT ConfigTreeViewWgt : public QWidget
{
	Q_OBJECT

public:
	ConfigTreeViewWgt(QWidget* parent, ValueComplex::PTR_T entry);
	ConfigTreeViewWgt(QWidget* parent);

	void setEntryNode(ValueComplex::PTR_T entry);

public slots:
	void onCopyParameterNameToClipboard();
	void onDeleteEntry();
	void onHideChilds();
	void onExpanded();

private:
	ConfigTreeItem* selectedItem();

	Ui::ConfigTreeViewWidget*			ui;

	ConfigTreeModel*					m_model;
};