#pragma once

#include <QVariant>
#include <QVector>

#include "value_complex.h"
#include "value_selection.h"

#include "config_tree_export.h"

class ConfigTreeModel;

class LIBCLASS_EXPORT ConfigTreeItem
{
    class ChildAddedConnection
    {
        public:

        ChildAddedConnection(const ValueBase::PTR_T src, std::function<void(ValueBase::PTR_T, ValueBase::PTR_T)>  callee)
        : m_source(std::move(src))
        , m_callee(std::move(callee))
        {
            m_source->addChildAddedCallback(&m_callee);
        }
		
		ChildAddedConnection& operator=(const ChildAddedConnection& other)
		{
			m_source = other.m_source;
			m_callee = other.m_callee;
			return *this;
		}

        ~ChildAddedConnection() 
        {
            m_source->removeChildAddedCallback(&m_callee);
        }

        private:
        std::function<void(ValueBase::PTR_T, ValueBase::PTR_T)>   m_callee;
        ValueBase::PTR_T                                          m_source;   
    };

    class ChildAddedConnector
    {
        private:
        std::vector<ChildAddedConnection>                               m_connections;

        public:

        void connect(ValueBase::PTR_T source, std::function<void(ValueBase::PTR_T, ValueBase::PTR_T)> callee)
        {
            m_connections.emplace_back(source, callee);
        }
    };

    class ChangeConnection
    {
        public:

        ChangeConnection(const ValueBase::PTR_T src, const std::function<void(ValueBase::PTR_T)> callee)
        : m_source(std::move(src))
        , m_callee(std::move(callee))
        {
            m_source->addValueChangedCallback(&m_callee);          
        }

		ChangeConnection& operator=(const ChangeConnection& other)
		{
			m_source = other.m_source;
			m_callee = other.m_callee;

			return *this;
		}

        ~ChangeConnection()
        {
            m_source->removeValueChangedCallback(&m_callee);
        }

        private:
        std::function<void(ValueBase::PTR_T)>                 m_callee;
        ValueBase::PTR_T                                      m_source;   
    };

    class ChangeConnector
    {
        private:
        std::vector<ChangeConnection>       m_connections;

        public:

        void connect(ValueBase::PTR_T source, std::function<void(std::shared_ptr<ValueBase>)> callee)
        {
            m_connections.emplace_back(source,callee);
        }
    };

public:
    explicit ConfigTreeItem(ConfigTreeModel* model,  ConfigTreeItem* parent = nullptr, ValueBase::PTR_T value = nullptr);

    ~ConfigTreeItem();

    bool isRootItem() const { return m_isRootItem; }

    ConfigTreeItem* child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data(ValueBase::PTR_T value) const;
    QVariant data(int column) const;
    ConfigTreeItem* appendChild();
  
    bool insertColumns(int position, int columns);
    ConfigTreeItem* parent();

    bool removeChildren();
    bool removeChildren(int position, int count);
    bool removeChild(ConfigTreeItem* child);
    bool removeColumns(int position, int columns);
    int childNumber() const;
    bool setData(int column, const QVariant& value);
    bool setData(const QVariant& value);
    ConfigTreeItem* appendChild(ValueBase::PTR_T value, ConfigTreeModel* model);
    ValueBase::PTR_T getValue() { return m_valueBase;  }

	void path(std::string& pathName);

	void appendChildXX(ValueBase::PTR_T node, ValueBase::PTR_T child);

private:
    bool														m_isRootItem;
    std::string													m_valueName;

    ChildAddedConnector                                         m_onChildAdded;
    ChangeConnector                                             m_onChanged;

    ConfigTreeModel*											m_model;

    bool														m_hideChilds;
    QVector<ConfigTreeItem*>									m_childItems;
    ConfigTreeItem*												m_parentItem;
    ValueBase::PTR_T                                            m_valueBase;

    void setSelectedData(ConfigTreeItem* item, ValueSelection::PTR_T selection);
};



