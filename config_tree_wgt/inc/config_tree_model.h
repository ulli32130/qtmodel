#pragma once

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include "value_complex.h"
#include "value_selection.h"

#include "config_tree_export.h"

class ConfigTreeItem;

class QTreeView;

class LIBCLASS_EXPORT ConfigTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    ConfigTreeModel(QObject* parent, ValueComplex::PTR_T entry);
    ~ConfigTreeModel();

    QVariant data(const QModelIndex &index, int role) const                                                         override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const                 override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const                         override;
    QModelIndex parent(const QModelIndex &index) const                                                              override;
    int rowCount(const QModelIndex &index = QModelIndex()) const                                                    override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const                                                override;
    Qt::ItemFlags flags(const QModelIndex &index) const                                                             override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole)                          override;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole)    override;

    bool insertColumns(int position, int columns, const QModelIndex &parent = QModelIndex())                        override;
    bool removeColumns(int position, int columns, const QModelIndex &parent = QModelIndex())                        override;
    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex())                              override; 
	bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex())                              override;

    void valueChanged(int row, int column, ConfigTreeItem* item);
	bool insertRows(int position, int rows, ConfigTreeItem* item);
    void removeChilds(ConfigTreeItem* item);
    void removeItem(int row, int column, ConfigTreeItem* parent);
    void removeNode(int row, ConfigTreeItem* parent);
    void setView(QTreeView* v);

signals:
    void databaseChanged();
    void childAdded(ValueBase::PTR_T parent, ValueBase::PTR_T child);

public slots:
    void onDataBaseChanged();
    void onChildAdded(ValueBase::PTR_T parent, ValueBase::PTR_T child);

private:
    ValueComplex::PTR_T  m_entry;

    void setupData(ValueComplex::PTR_T entry, ConfigTreeItem* parent);
    ConfigTreeItem		*getItem(const QModelIndex &index) const;
    ConfigTreeItem		*m_rootItem;
    QTreeView*			 m_treeView;
};

