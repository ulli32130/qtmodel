#pragma once

#include <QStyledItemDelegate>
#include <QLineEdit>

#include "config_tree_export.h"

enum class GuiEditorType : uint32_t {
	PathSelection = 1,
	FileSelection
};

class LIBCLASS_EXPORT ConfigTreeSelectFile : public QLineEdit
{
	Q_OBJECT

public:

	enum class State {
		Init,
		DialogDone
	};

	ConfigTreeSelectFile(QWidget *parent = nullptr);

	State& getState() { return m_state; }
	void setState(State s) { m_state = s; }

private:
	State		m_state;
};

class LIBCLASS_EXPORT ConfigTreeSelectPath : public QLineEdit
{
	Q_OBJECT

public:

	enum class State {
		Init,
		DialogDone
	};

	ConfigTreeSelectPath(QWidget *parent = nullptr);

	State& getState()			{ return m_state;	}
	void setState(State s)		{ m_state = s;		}

private:
	State		m_state;

};


class LIBCLASS_EXPORT ConfigTreeDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:

    using QStyledItemDelegate::QStyledItemDelegate;

    void paint(QPainter* painter, const QStyleOptionViewItem& option,
        const QModelIndex& index)                                                       const override;

    QSize sizeHint(const QStyleOptionViewItem& option,
        const QModelIndex& index)                                                       const override;

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
        const QModelIndex& index)                                                       const override;

    void setEditorData(QWidget* editor, const QModelIndex& index)                       const override;

    void setModelData(QWidget* editor, QAbstractItemModel* model,
        const QModelIndex& index)                                                       const override;

public slots:
    void commitAndCloseEditor();
    void onCurrentIndexChanged(int n);
};